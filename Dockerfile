FROM m0rf30/arch-yay
ENV NVIDIA_VISIBLE_DEVICES all
ENV NVIDIA_DRIVER_CAPABILITIES compute,utility


RUN sudo pacman -Sy --noprogressbar --noconfirm nvidia-utils clinfo libpng libpng12 librsvg qt5-svg
RUN BUILDDIR=/tmp/makepkg MAKEFLAGS="-j7"  /bin/bash -c "sudo -u user  yay -Sy --noconfirm --nodiffmenu mandelbulber2-opencl-git"
RUN /bin/bash -c "sudo pacman -Sy --noprogressbar --noconfirm opencl-nvidia"

CMD [ "mandelbulber2", "-g", "-n" ]